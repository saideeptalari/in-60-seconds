# Begininng Computer Vision

---
@snap[north-west]
## Beginning Computer Vision
@snapend

@ul
- Transfer color between images.
- Terrain extraction from point cloud. 
@ulend

---
@snap[north]
## Photos and Color
@snapend

@ul
 - A photo is simply light reflected by an object and captured by a camera.
 - Color is a mechanism of the human eye by which it perceives a mixture of lights of different wavelengths.
@ulend

---
@snap[north-west]
## How photos are captured?
@snapend

@img[fragment](images/cam_meme1.jpg)

---
@snap[north-west]
## How photos are captured?
@snapend

@img[fragment](images/cam_sensor.png)

---
@snap[north]
## OpenCV
@snapend

@ul
- Exhaustive library for image processing and computer vision.
- Highly optimised.
- Several language bindings.
- Portable.
- `sudo apt-get install python-opencv` (or) `pip install opencv-python`.
@ulend

---?code=code/basics.py&lang=python&title=getting hands dirty
@[1](import opencv library)
@[3-5](read the image from disk)
@[13-14](shape of image)

---
@snap[north]
## Numpy
@snapend

@img[fragment](images/np_array.png)

---
@snap[north]
## Flynn's Taxonomy
@snapend

@ul
 - SISD
 - SIMD
 - MISD
 - MIMD
 - hell with the loops!
@ulend

---
@snap[north]
## SISD - SIMD
@snapend

@div[left-50]
<br><br>
![sisd](images/sisd.png)
@divend

@div[right-50 fragment]
<br><br>
![sisd](images/simd.png)
@divend

---
@snap[north]
## Color spaces
@snapend

@ul
- RGB
- HSV
- YCrCb
- LAB
@ulend

---
![](images/rgb.png)

---
@snap[north]
## RGB
@snapend

![](images/capsi_rgb.PNG)

---
![](images/hsv.png)

---
@snap[north]
## HSV
@snapend

![](images/capsi_hsv.png)

---
![](images/ycrcb.png)

---
@snap[north]
## YCrCb
@snapend

![](images/capsi_ycrcb.png)

---
![](images/lab.jpg)

---
@snap[north]
## LAB
@snapend

![](images/capsi_lab.png)

---?code=code/basics.py&lang=python&title=converting between colorspaces
@[16-19](convert image)
@[20-24](display images)

---
@snap[north]
## Color transfer
@snapend
![](images/color_transfer_output.png)

---?code=code/color_transfer.py&lang=python&title=color transfer
@[1-10](load and convert to LAB)
@[13-17](compute image statistics)
@[20](the single line to transfer color)
@[21-29](convert back to RGB and display)

---
@snap[north]
## Terrain extraction
@snapend
![](images/terr_ext.png)

---?code=code/extract_terrain.py&lang=python&title=terrain extraction
@[13](the single line to extract terrain)

---
# ~|Q|