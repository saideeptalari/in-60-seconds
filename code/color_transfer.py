import cv2
import numpy as np 

# load images
src_image = cv2.imread('../images/ct_src.jpg')
dst_image = cv2.imread('../images/ct_dst.jpg')

# convert to LAB
src_lab = cv2.cvtColor(src_image, cv2.COLOR_BGR2LAB)
dst_lab = cv2.cvtColor(dst_image, cv2.COLOR_BGR2LAB)

# compute required statistics
src_mean = src_lab.mean(axis=(0, 1))
dst_mean = dst_lab.mean(axis=(0, 1))

src_std = src_lab.std(axis=(0, 1))
dst_std = dst_lab.std(axis=(0, 1))

# color transfer
output_lab = ((dst_lab - dst_mean) * (dst_std/src_std)) + src_mean
output_lab = np.clip(output_lab, 0, 255).astype("uint8")

output_rgb = cv2.cvtColor(output_lab, cv2.COLOR_LAB2BGR)

# display the output
output = np.hstack([src_image, dst_image, output_rgb])
cv2.imshow("Output", output)
cv2.waitKey(0)
cv2.destroyAllWindows()