import cv2

# load the image
image = cv2.imread("../images/capsicum.jpg")

# display the image
cv2.imshow("Image", image)

# wait for infinite time and read the input key
cv2.waitKey(0)
cv2.destroyAllWindows()

print("Image size (height, width, channels)", image.shape)
print(image)

#converting between colorspaces
image_hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)  # to 180%
image_ycrcb = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)
image_lab = cv2.cvtColor(image, cv2.COLOR_BGR2LAB)

cv2.imshow("RGB", image)
cv2.imshow("HSV", image_hsv)
cv2.imshow("YCrCb", image_ycrcb)
cv2.imshow("LAB", image_lab)

cv2.waitKey(0)
cv2.destroyAllWindows()