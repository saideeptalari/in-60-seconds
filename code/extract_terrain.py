import laspy
import cv2
import numpy as np
import sys

lf = laspy.file.File(sys.argv[1])
img = np.dstack([lf.blue, lf.green, lf.red])
img /= 256
img = img.astype("uint8")

img_hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

indices = np.where(cv2.inRange(img_hsv, (20, 0, 0), (255, 255, 255)).astype('bool') != True)[1]

pts = lf.points[indices]

wf = laspy.file.File(sys.argv[2], mode="w", header=lf.header)
wf.points = pts
wf.close()